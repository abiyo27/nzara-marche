# Choose and name our temporary image.
FROM alpine as intermediate
# Add metadata identifying these images as our build containers (this will be useful later!)
LABEL stage=intermediate

# Install dependencies required to git clone.
RUN apk update && \
    apk add --update git && \
    apk add --update openssh

# 1. Create the SSH directory.
# 2. Populate the private key file.
# 3. Set the required permissions.
# 4. Add github to our list of known hosts for ssh.
#RUN mkdir -p /root/.ssh/ && \
#    echo "$SSH_KEY" > /root/.ssh/id_rsa && \
#    chmod -R 600 /root/.ssh/ && \
#    ssh-keyscan -t rsa github.com >> ~/.ssh/known_hosts

# Clone a repository (my website in this case)
RUN git clone https://gitlab.com/abiyo27/nzara-marche.git#main

FROM wordpress:latest
COPY --from=intermediate /nzara-marche/wordpress /var/www/html
